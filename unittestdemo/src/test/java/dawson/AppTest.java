package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dawson.App;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    @Test
    public void theEchoMethod()
    {
        App ob = new App();
        int expected = 5;
        int actual = ob.echo(5);
        String message = "Testing echo of 5";
        assertEquals(message, expected , actual);
    }
    @Test
    public void theAddMore()
    {
        App ob = new App();
        int expected = 6;
        int actual = ob.oneMore(5);
        String message = "Testing echo of 5";
        assertEquals(message, expected , actual);
    }
}
